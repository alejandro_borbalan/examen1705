<?php

use Illuminate\Database\Seeder;

class ExamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 100; $i++) {
            $exam = factory(App\Exam::class)->create();
            $questions = \App\Question::all()->random(20);
            foreach ($questions as $question) {
                $exam->questions()->attach($question);
            }

        }

    }
}
