@extends('layouts.app')

@section('content')

<div class="container">

<h1>Lista de Preguntas</h1>

    <table class="table table-bordered">
    <tr>
        <th>Enuciado</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
    </tr>
    @foreach ($questions as $question)
    <tr>
        <td>{{ $question->enunciado }}</td>
        <td>{{ $question->A }}</td>
        <td>{{ $question->B }}</td>
        <td>{{ $question->C }}</td>
        <td>{{ $question->D }}</td>
        <td>

            <form method="post" action="/questions/{{ $question->id }}">
            <a class="btn btn-success" href="/questions/remember/{{ $question->id }}">Recordar</a>


            <a class="btn btn-info" href="/questions/{{ $question->id }}">Ver</a>


            </form>
        </td>
    </tr>
    @endforeach
</table>
{{ $questions->links() }}



    </div>
</div>
@endsection
