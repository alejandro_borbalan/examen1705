@extends('layouts.app')

@section('content')

<div class="container">

<h1>Lista de Examenes</h1>

    <table class="table table-bordered">
    <tr>
        <th>Título</th>
        <th>Nombre de Usuario</th>
        <th>Nombre del Modulo</th>
        <th>Acciones</th>
    </tr>
    @foreach ($exams as $exam)
    <tr>
        <td>{{ $exam->title }}</td>
        <td>{{ $exam->user->name }}</td>
        <td>{{ $exam->module->name }}</td>
        <td>

            <form method="post" action="/exams/{{ $exam->id }}">
            <a class="btn btn-success" href="/exams/remember/{{ $exam->id }}">Recordar</a>


            <a class="btn btn-info" href="/exams/{{ $exam->id }}">Ver</a>

            <a class="btn btn-info" href="/exams/{{ $exam->id }}/edit">Editar</a>
            @can('delete',$exam)
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input class="btn btn-danger" type="submit" value="borrar">
            @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>
{{ $exams->links() }}

@can ('create', App\Exam::class)
<a href="/articles/create" class="btn btn-info">Nuevo</a>
@endcan

<hr>

    <div>
        <h3>Lista de Examenes</h3>
        <ul>
        @if (session()->has('exams'))
        @foreach (session()->get('exams') as $exam)
            <li>{{ $exam->title }}</li>
        @endforeach
        @endif
        </ul>
    </div>

    </div>

</div>


    </div>
</div>
@endsection
