<?php

namespace App\Policies;

use App\User;
use App\Exam;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin())
        {
            return true;
        }
    }

    public function view(User $user, Exam $exam)
    {

    }

    public function create(User $user)
    {

    }

    public function update(User $user, exam $exam)
    {
    }

    public function delete(User $user, Exam $exam)
    {

        if($user->name == $exam->user){
            return true;
        }
    }

    public function index(User $user)
    {
        return true;
    }
}
