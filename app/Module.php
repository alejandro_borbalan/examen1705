<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{

    protected $fillable = [
        'code','name'
    ];

    public function exam(){
       return $this->hasMany('App\Exam');
    }

    public function questions(){
        return $this->hasMany('App\Question');
    }
}
