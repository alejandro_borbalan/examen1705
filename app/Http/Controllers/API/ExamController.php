<?php

namespace App\Http\Controllers\API;

use App\Exam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{
    public function index()
    {
        $exams = Exam::all();
        return $exams;
    }

    public function show($id)
    {
        $exams = Exam::find($id);
        if ($exams) {
            return $exams;
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }
    public function update($id)
    {

    }
    public function destroy($id)
    {

    }
    public function store(Request $request)
    {

    }
    public function edit($id)
    {

    }

}
