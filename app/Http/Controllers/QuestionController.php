<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $questions = Question::paginate(10);
        return view('questions.index', ['questions' => $questions]);
    }

    public function create()
    {
        return view('questions.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,
        [
            'code' => 'required|unique:questions|max:6',
            'name' => 'required|max:255',
            'price' => 'required|numeric'
        ]);

        $this->authorize('create', \App\Question::class);

        $questions = new Question();
        $question->fill($request->all());
        $question->code = $request->input('code');
        $question->save();
        return redirect('/questions');
    }

    public function show($id)
    {
        $question = Question::find($id);
        $this->authorize('view', $question);

        return view('questions.show', ['question' => $question]);
    }

    public function edit($id)
    {
        $question = Question::where('id', $id)->first();
        $this->authorize('update', $question);
        return view('questions.edit', ['question' => $question]);
    }

    public function update(Request $request, $id)
    {
        $question = Question::find($id);
        $this->authorize('update', $question);
        $question->fill($request->all());
        $question->save();
        return redirect('/exams');
    }

    public function destroy($id)
    {
        $question = Question::find($id);
        $this->authorize('delete', $question);

        $question->delete();
        return redirect('/exams');
    }


}
