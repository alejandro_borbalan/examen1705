<?php

namespace App\Http\Controllers;

use App\Exam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ExamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $exams = Exam::paginate(10);
        return view('exams.index', ['exams' => $exams]);
    }

    public function create()
    {
        return view('exams.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,
        [
            'code' => 'required|unique:exams|max:6',
            'name' => 'required|max:255',
            'price' => 'required|numeric'
        ]);

        $this->authorize('create', \App\Exam::class);

        $exams = new Exam();
        $exam->fill($request->all());
        $exam->code = $request->input('code');
        $exam->save();
        return redirect('/exams');
    }

    public function show($id)
    {
        $exam = Exam::find($id);
        $this->authorize('view', $exam);

        return view('exams.show', ['exam' => $exam]);
    }

    public function edit($id)
    {
        $exam = Exam::where('id', $id)->first();
        $this->authorize('update', $exam);
        return view('exams.edit', ['exam' => $exam]);
    }

    public function update(Request $request, $id)
    {
        $exam = Exam::find($id);
        $this->authorize('update', $exam);
        $exam->fill($request->all());
        $exam->save();
        return redirect('/exams');
    }

    public function destroy($id)
    {
        $exam = Exam::find($id);
        $this->authorize('delete', $exam);

        $exam->delete();
        return redirect('/exams');
    }

    public function remember($id)
    {
        $exam = Exam::find($id);
        session()->push('exams', $exam);


        if (session()->has("basket.$id")) {
            $amount = session()->get("basket.$id.amount");
            $amount++;
            session()->put("basket.$id.amount", $amount);
        } else {
            session()->put("basket.$id.exam", $exam);
            session()->put("basket.$id.amount", 1);
        }
        return back();
    }
}
