<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{

    protected $fillable = [
        'date','title', 'module_id', 'user_id'
    ];

    public function module(){
        return $this->belongsTo('App\Module','module_id');
    }

    public function questions(){
        return $this->belongsToMany('App\Question');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
